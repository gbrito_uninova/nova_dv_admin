# Double Virtualization (DV)
Software module for networked solutions for automatic management of assets and processes.



## DV Administration & Management Component
This repository contains the necessary files for installing and running the **Administration & Management (DVA&M)** module of the Double Virtualization.

The DVA&M component is resposible for monitoring and administration a set of assets.

---
## Requirements
- **node.js** (v10 and v12 tested)

The DV application runs as a **node.js** application.

The solution as been tested on *Windows*, *MacOS* and *Unix* systems.

---
## Install

To install the DVA&M:

1. Browse into the repository directory
2. execute the install script, as **node.js application**
	- *$node install_dv_admin.js*

The script creates one directory for the application files and one for the runtime files, both relative to the current account HOME directory:

- *$~/nova_dv/dv_admin* - application directory  
- *$~/.nova_dv/dv_admin* - runtime directory

---
## Setup and run

Browse into the application directory

### Setup files

Edit the file with the runtime configuration

- **dv_admin_settings.js**
	- host(s): *default:* 0.0.0.0 (all available IPV4)
	- port *default:* 1888
	- HTTPS (include certificate paths)
	- ...

### execution

execute the node app **dv_admin.js**

- *$node dv_admin.js*

---
## Authors
- Guilherme Brito
- Giovanni di Orio
- Pedro Mal�

Uninova / Nova School of Science (Lisbon)
