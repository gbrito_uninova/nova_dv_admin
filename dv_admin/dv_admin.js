#!/usr/bin/env node



var path = require("path");
var nopt = require("nopt");
var os = require("os");
var fs = require("fs-extra");
var express = require("express");
var https = require('https');
var http = require('http');
var crypto = require("crypto");
var bcrypt = require('bcryptjs');
var RED = require('node-red');
var request = require('request-promise');
var url = require('url');



console.log("DV start up!!")
//Check if System homedir is defined
if (os.homedir() === null) {
    //If not defined, DV cant be instantiated
    console.log("Unsupported System or Platform error - Undefined home directory");
    console.log("Exiting application...");
    process.exit(1);
}

//No use here. If https ca not defined, this variable needs to be set before launching main script
//process.env['NODE_EXTRA_CA_CERTS'] = path.join(os.homedir(),'dv','.certs','CA','rootCA.pem');

var server;
var app = express();

var mainDir = path.join(os.homedir(), "nova_dv", "dv_admin");
var runtimeDir = path.join(os.homedir(), ".nova_dv", ".dv_admin");
var flowFile = path.join(runtimeDir, "dv_admin_flows.json");
var redSettingsFile = path.join(mainDir, "dv_admin_nodered_settings.js");
//var adminSettingsFile=path.join(mainDir, "dv_admin_config.js");

var knownOpts = {
    "help": Boolean,
    "port": Number,
    "verbose": Boolean,
    "safe": Boolean
};
var shortHands = {
    "h": ["--help"],
    "p": ["--port"],
    "v": ["--verbose"]
};
nopt.invalidHandler = function (k, v, t) {
    // TODO: console.log(k,v,t);
}
var parsedArgs = nopt(knownOpts, shortHands, process.argv, 2)
if (parsedArgs.help) {
    console.log("Usage: DVadmin [-v] [--port PORT][--safe]");
    console.log("");
    console.log("Options:");
    console.log("  -p, --port     PORT  port to listen on");
    console.log("  -v, --verbose        enable verbose output");
    console.log("      --safe           enable safe mode");
    console.log("  -h, --help           show this help");
    console.log("");
    process.exit();
}

process.env.NODE_RED_HOME = process.env.NODE_RED_HOME || path.join(__dirname, "node_modules", "node-red");


/*
Check is runtimeDir exists
If not, it tries to create the dir: HOME/.defender/.dv/admin
TODO: Close app in order to the admin be able to run instalation script
*/
if (!fs.pathExistsSync(runtimeDir)) {
    //fs.mkdirsSync(runtimeDir);
    //console.log("Created runtimeDir: "+runtimeDir);
    console.log("Run install_dv_admin.js before launching app..");
    process.exit(3);
}






/*
Check settings file
Instaltion Script should install all
If not present, it can end process or copy a default settings
*/
if (!fs.existsSync(redSettingsFile)) {
    /*if(fs.existsSync(path.join(__dirname,"dv_admin_nodered_settings.js"))){
        fs.copyFileSync(path.join(__dirname,"dv_admin_nodered_settings.js"),redSettingsFile);
        console.log("Load default settings for node-red");
    }else{
        console.log("No settings file for launching node-red instance");
        console.log("Instalation Script should be run before launching app..");
        process.exit(3);
    }*/
    console.log("No settings file for launching node-red instance");
    console.log("Instalation Script should be run before launching app..");
    process.exit(3);
}



/*////////////////
Script section for preparing settings for node-red
Based on node-red module, but adopted for DV case (mandatory HTTPS, port, etc.)
*/

try {
    //load settings to from file into cache
    var settings = require(redSettingsFile);
    settings.settingsFile = redSettingsFile;
} catch (err) {
    console.log("Error loading settings file: " + redSettingsFile)
    if (err.code == 'MODULE_NOT_FOUND') {
        if (err.toString().indexOf(redSettingsFile) === -1) {
            console.log(err.toString());
        }
    } else {
        console.log(err);
    }
    process.exit();
}

// Verbose and safe mode to be checked
if (parsedArgs.verbose) {
    settings.verbose = true;
}
if (parsedArgs.safe) {
    settings.safeMode = true;
}

//Ensure https is on on red settings
if (!settings.https || !settings.https.key || !settings.https.cert) {
    console.log("HTTPS not correctly defined on red_settings file");
    console.log("starting on http mode");
    //process.exit(1);
    //load HTTP server
    try {
        //    server = http.createServer(app);
        settings.requireHttps = false;
        settings.https = false;
        server = http.createServer(app);
        server.setMaxListeners(0);
    } catch (err) {
        console.log("Error ocurred while creating http server");
        console.log(err);
        process.exit(4);
    }


} else {
    //load HTTPS server
    try {
        //    server = http.createServer(app);
        server = https.createServer(settings.https, app);
        //server = https.createServer(settings.https,function(req,res) {app(req,res);});
        server.setMaxListeners(0);
    } catch (err) {
        console.log("Error ocurred while creating https server");
        console.log(err);
        process.exit(4);
    }
}

//Look for port specified by user
if (parsedArgs.port) {
    console.log("Custom port has been declared: " + parsedArgs.port);
    settings.uiPort = parsedArgs.port;
} else {
    console.log("Using default port: 1888");
    //settings.uiPort=1888; 
}

//This will set all IPv4 interfaces to listen, or a given location if defined in red_settings file
settings.uiHost = settings.uiHost || "0.0.0.0";


///////////////////////////////////
//TODO: CHECK THIS
settings.httpRoot = false;
if (settings.httpAdminRoot !== false) {
    settings.httpAdminRoot = formatRoot(settings.httpAdminRoot || "/");
    settings.httpAdminAuth = settings.httpAdminAuth || settings.httpAuth;
} else {
    settings.disableEditor = true;
}
if (settings.httpNodeRoot !== false) {
    settings.httpNodeRoot = formatRoot(settings.httpNodeRoot || "/");
    settings.httpNodeAuth = settings.httpNodeAuth || settings.httpAuth;
} else {
    settings.httpNodeRoot = formatRoot("defender");
}
settings.flowFile = flowFile;
settings.userDir = runtimeDir;
///////////////////////
settings.functionGlobalContext = {
    args: process.env,
    path: path,
    fs: fs,
    os: os,
    request: request,
    url: url
    //node_args:process.env.node_args,
    //admin_comm:process.env.ADMIN_COMM,
}



//TODO: check if this os needed
//for migration to use specific nodes??? - on dv_asset
//settings.nodesDir=path.join(__dirname,"red_lib");
//
try {
    RED.init(server, settings);
} catch (err) {
    if (err.code == "unsupported_version") {
        console.log("Unsupported version of node.js:", process.version);
        console.log("Node-RED requires node.js v4 or later");
    } else if (err.code == "not_built") {
        console.log("Node-RED has not been built. See README.md for details");
    } else {
        console.log("Failed to start server:");
        if (err.stack) {
            console.log(err.stack);
        } else {
            console.log(err);
        }
    }
    process.exit(1);
}

if (settings.httpAdminRoot !== false) {
    app.use(settings.httpAdminRoot, RED.httpAdmin);
}
if (settings.httpNodeRoot !== false) {
    app.use(settings.httpNodeRoot, RED.httpNode);
}

/* ///// WHAT IS STATIC IN HERE????
    if (settings.httpStatic) {
    settings.httpStaticAuth = settings.httpStaticAuth || settings.httpAuth;
    if (settings.httpStaticAuth) {
        app.use("/",basicAuthMiddleware(settings.httpStaticAuth.user,settings.httpStaticAuth.pass));
    }
    app.use("/",express.static(settings.httpStatic));
}
*/


RED.start().then(function () {
    console.log("Starting Node_red");
    if (settings.httpAdminRoot !== false || settings.httpNodeRoot !== false || settings.httpStatic) {
        server.on('error', function (err) {
            if (err.errno === "EADDRINUSE") {
                RED.log.error(RED.log._("server.unable-to-listen", { listenpath: getListenPath() }));
                RED.log.error(RED.log._("server.port-in-use"));
            } else {
                RED.log.error(RED.log._("server.uncaught-exception"));
                if (err.stack) {
                    RED.log.error(err.stack);
                } else {
                    RED.log.error(err);
                }
            }
            process.exit(1);
        });
        server.listen(settings.uiPort, settings.uiHost, function () {
            if (settings.httpAdminRoot === false) {
                RED.log.info(RED.log._("server.admin-ui-disabled"));
            }
            settings.serverPort = server.address().port;
            process.title = parsedArgs.title || 'node-red';
            RED.log.info(RED.log._("server.now-running", { listenpath: getListenPath() }));
        });
    } else {
        RED.log.info(RED.log._("server.headless-mode"));
    }
}).otherwise(function (err) {
    RED.log.error(RED.log._("server.failed-to-start"));
    if (err.stack) {
        RED.log.error(err.stack);
    } else {
        RED.log.error(err);
    }
});



function getListenPath() {
    var port = settings.serverPort;
    if (port === undefined) {
        port = settings.uiPort;
    }

    var listenPath = 'http' + (settings.https ? 's' : '') + '://' +
        (settings.uiHost == '::' ? 'localhost' : (settings.uiHost == '0.0.0.0' ? '127.0.0.1' : settings.uiHost)) +
        ':' + port;
    if (settings.httpAdminRoot !== false) {
        listenPath += settings.httpAdminRoot;
    } else if (settings.httpStatic) {
        listenPath += "/";
    }
    return listenPath;
}




////////////////AUX FUNCTIONS

/*
basic auth for node-red editor
This is a copy from Node-red, but can be modified
*/
function basicAuthMiddleware(user, pass) {
    var basicAuth = require('basic-auth');
    var checkPassword;
    var localCachedPassword;
    if (pass.length == "32") {
        // Assume its a legacy md5 password
        checkPassword = function (p) {
            return crypto.createHash('md5').update(p, 'utf8').digest('hex') === pass;
        }
    } else {
        checkPassword = function (p) {
            return bcrypt.compareSync(p, pass);
        }
    }

    var checkPasswordAndCache = function (p) {
        // For BasicAuth routes we know the password cannot change without
        // a restart of Node-RED. This means we can cache the provided crypted
        // version to save recalculating each time.
        if (localCachedPassword === p) {
            return true;
        }
        var result = checkPassword(p);
        if (result) {
            localCachedPassword = p;
        }
        return result;
    }

    return function (req, res, next) {
        if (req.method === 'OPTIONS') {
            return next();
        }
        var requestUser = basicAuth(req);
        if (!requestUser || requestUser.name !== user || !checkPasswordAndCache(requestUser.pass)) {
            res.set('WWW-Authenticate', 'Basic realm="Authorization Required"');
            return res.sendStatus(401);
        }
        next();
    }
}

//formating https routes: adding "/" before and after
function formatRoot(_root) {
    if (_root[0] != "/") {
        _root = "/" + _root;
    }
    if (_root.slice(-1) != "/") {
        _root = _root + "/";
    }
    return _root;
}

process.on('uncaughtException', function (err) {
    util.log('[DV] Uncaught Exception:');
    if (err.stack) {
        util.log(err.stack);
    } else {
        util.log(err);
    }
    process.exit(1);
});

process.on('SIGINT', function () {
    RED.stop().then(function () {
        process.exit();
    });
});

//console.log("Ending DV launch script");