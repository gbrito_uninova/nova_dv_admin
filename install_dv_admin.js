#!/usr/bin/env node

var os = require("os");
var path = require('path');
var fs = require('fs-extra');

console.log("DV_Admin install start up!!")

//Check if system has a HOME dir settled
if (os.homedir() === null) {
    //If not defined, DV cant be instantiated
    console.log("Unsupported System or Platform error - Undefined home directory");
    console.log("Exiting application...");
    process.exit(1);
}

var DV_DIR = path.join(os.homedir(), "nova_dv");
var DV_ADMIN_DIR = path.join(DV_DIR, "dv_admin");
var DV_RUNTIME_DIR = path.join(os.homedir(), ".nova_dv");
var DV_ADMIN_RUNTIME_DIR = path.join(DV_RUNTIME_DIR, ".dv_admin");

runinstall();

async function runinstall() {

    try {
        //create necessary directories
        dirsAndFiles();

        //run npm install for both app and runtime direcories
        //TODO check if anything fails
        //need to capture the correct failure
        console.log("Starting npm install in: " + DV_ADMIN_DIR);
        await makeNpmInstall(DV_ADMIN_DIR);
        
        console.log("Starting npm install in: " + DV_ADMIN_RUNTIME_DIR);
        await makeNpmInstall(DV_ADMIN_RUNTIME_DIR);
        console.log("installation concluded");

    } catch (err) {
        console.log("installation not concluded!! terminated with error: ");
        console.log(err);
        process.exit(1);
    }
};


/*
npm Install async with promise and logs
*/
function makeNpmInstall(dir) {
    const exec = require('child_process').spawn;
    return new Promise(async (resolve, reject) => {
        const ls = exec('npm', ['install'], { cwd: dir });
        ls.stdout.on('data', (data) => {
            console.log(data.toString());
        });
        ls.stderr.on('data', (data) => {
            console.log(data.toString());
        });
        ls.on('close', (code) => {
            resolve(ls);
        });
        ls.on('error', (err) => {
            reject(err);
        });
    });
}


/*
Remake directories for dv_admin application, dv_admin runtime, and copy CA cert
*/
function dirsAndFiles() {

    //application directory
    fs.emptyDirSync(DV_ADMIN_DIR);
    fs.copySync(path.join(__dirname, "dv_admin"), DV_ADMIN_DIR);

    //runtime directory
    //TODO: backup old flow, settings and node_modules folder??
    fs.emptyDirSync(DV_ADMIN_RUNTIME_DIR);
    fs.copySync(path.join(__dirname, ".dv_admin"), DV_ADMIN_RUNTIME_DIR);
}

